import 'dart:async';

import 'package:app_group_directory/app_group_directory.dart';
import 'package:flutter/material.dart';
import 'package:hive_flutter/hive_flutter.dart';

BoxCollection? collection;

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();

  final directory = await AppGroupDirectory.getAppGroupDirectory(
      'group.com.example.hiveTestApp');
  print(directory?.path);
  Hive.init(directory?.path);

  collection = await BoxCollection.open(
    'hive_test',
    {'box'},
    // When using [AppGroupDirectory], automatic path resolution does not work
    // as supposed and we therefore need to provide the resolved
    // group path + the subdirectory for the [BoxCollection] as manual [path].
    // This is a restriction due to iOS security policies as path lookup and
    // resolution accesses paths not permitted in the app group.
    path: '${directory?.path ?? '.'}/hiveTestCollection',
  );

  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        colorScheme: ColorScheme.fromSeed(seedColor: Colors.deepPurple),
        useMaterial3: true,
      ),
      home: const MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key});

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  CollectionBox? testBox;

  final StreamController<int> _valueStreamController =
      StreamController.broadcast();

  late Stream<int> _boxStream;

  @override
  void initState() {
    _boxStream = _valueStreamController.stream;
    _openBox();
    super.initState();
  }

  Future<int> _getValue() async => (await testBox?.get('counter')) as int? ?? 0;

  Future<void> _incrementCounter() async {
    final count = await _getValue();
    await testBox?.put('counter', count - 1);
    _valueStreamController.add(await _getValue());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Theme.of(context).colorScheme.inversePrimary,
        title: const Text('Hive Group Dir'),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'You have pushed the button this many times:',
            ),
            StreamBuilder(
                stream: _boxStream,
                builder: (context, value) {
                  return Text(
                    '$value',
                    style: Theme.of(context).textTheme.headlineMedium,
                  );
                }),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ),
    );
  }

  Future<void> _openBox() async {
    testBox = await collection?.openBox('box');
    _valueStreamController.add(await _getValue());
  }
}
